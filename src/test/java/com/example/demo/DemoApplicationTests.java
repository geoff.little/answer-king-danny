package com.example.demo;

import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.dto.OrdersDTO;
import com.example.demo.entity.Item;
import com.example.demo.entity.OrderItems;
import com.example.demo.entity.Orders;
import com.example.demo.repository.ItemRepository;
import com.example.demo.repository.OrderItemsRepository;
import com.example.demo.repository.OrdersRepository;
import com.example.demo.service.OrdersService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@InjectMocks
	private OrdersService orderService;

	@Mock
	private OrdersRepository ordersRepository;

	@Mock
	private ItemRepository itemRepository;

	@Mock
	private OrderItemsRepository orderItemsRepository;
	
	private static final Long itemId = 1L;
	private static final Long orderId = 1L;
	private static final int quantity = 5;
	private static final String itemName = "itemOne";
	
//	@Test
//	public void testAddItem() {
//		
//		Orders order = new Orders();
//		Optional<Orders> ordersOptional = Optional.of(order);
//		when(ordersRepository.findById(orderId)).thenReturn(ordersOptional);
//		
//		Item item = new Item();
//		item.setName(itemName);
//		Optional<Item> itemOptional = Optional.of(item);
//		when(itemRepository.findById(itemId)).thenReturn(itemOptional);
//		
//		OrderItems orderItem = new OrderItems();
//		when(orderItemsRepository.findByOrderIdAndName(orderId, itemName)).thenReturn(orderItem);
//		
//		OrdersDTO orderDTO = orderService.addItem(itemId, orderId, quantity);
//		
//	}
}
