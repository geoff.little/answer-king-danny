package com.example.demo.entity;

import java.beans.Transient;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;

import org.hibernate.annotations.Type;

import com.example.demo.dto.OrderItemsDTO;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Orders {

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id private Long id;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private LocalDateTime orderDateTime;
	
	private String orderStatus;
	
	@Min(0) 
	private BigDecimal amountPaid;

	@OneToMany(mappedBy="order")
	private List<OrderItems> orderItems = new ArrayList<>();
	
	@Type(type = "numeric_boolean")
	private boolean promoCodeUsed = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getOrderDateTime() {
		return orderDateTime;
	}

	public void setOrderDateTime(LocalDateTime orderDateTime) {
		this.orderDateTime = orderDateTime;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	public BigDecimal getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}
	
	public List<OrderItems> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<OrderItems> orderItems) {
		this.orderItems = orderItems;
	}
	
	public List<OrderItemsDTO> getOrderItemsDetails(){
		
		List<OrderItemsDTO> orderItemsList = new ArrayList<>();
		
		for(OrderItems orderItem : orderItems) {
			OrderItemsDTO newOrderItems = new OrderItemsDTO();
			
			newOrderItems.setId(orderItem.getId());
			newOrderItems.setName(orderItem.getName());
			newOrderItems.setDescription(orderItem.getDescription());
			newOrderItems.setPrice(orderItem.getPrice());
			newOrderItems.setQuantity(orderItem.getQuantity());
			newOrderItems.setSubTotal(orderItem.getSubTotal());
			
			orderItemsList.add(newOrderItems);
		}
		
		return orderItemsList;
	}
	
	public boolean isPromoCodeUsed() {
		return promoCodeUsed;
	}

	public void setPromoCodeUsed(boolean promoCodeUsed) {
		this.promoCodeUsed = promoCodeUsed;
	}
	
	@Transient
	public BigDecimal getTotalPrice() {
		
		BigDecimal orderTotal = new BigDecimal("0");
		
		for(OrderItems orderItem : orderItems) {
			
			orderTotal = orderTotal.add(orderItem.getSubTotal());
	
		}
		
		return orderTotal;
	}
	
	@Transient
	public BigDecimal getChange(BigDecimal totalPrice, BigDecimal amountPaid) {
		
		BigDecimal totalChange = new BigDecimal("0");
		
		BigDecimal calculatedChange = totalPrice.subtract(amountPaid);
		
		if (calculatedChange.compareTo(BigDecimal.ZERO) < 0) { 
			return totalChange = calculatedChange.abs(); 
		}
		return totalChange;
	}
}
