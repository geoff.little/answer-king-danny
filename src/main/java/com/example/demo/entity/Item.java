package com.example.demo.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.example.demo.dto.CategoryDTO;

@Entity
public class Item {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id private Long id;
	
	@NotBlank
	@Size(min=3, max=50)
	private String name;
	
	@NotBlank
	@Size(min=10, max=150)
	private String description;
	
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "category_item", 
        joinColumns = { @JoinColumn(name = "item_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "cat_id") }
    )
	private List<Category> categories;
	

	@Min(0) 
	private BigDecimal price;
	
	public Long getId() {
		return id;
	}
	
	public List<Long> getCategoryIds(){
		
		List<Long> categoryIds = new ArrayList<>();
		
		for(Category category : categories) {
			
			categoryIds.add(category.getId());
		}
		
		return categoryIds;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<CategoryDTO> getCategoryDetails() {
		
		List<CategoryDTO> categoryList = new ArrayList<>();
		
		for(Category category : categories) {
			CategoryDTO newCategory = new CategoryDTO();
			
			newCategory.setId(category.getId());
			newCategory.setName(category.getName());
			newCategory.setDescription(category.getDescription());
			newCategory.setItemDetails(category.getItemDetails());
			
			categoryList.add(newCategory);
		}
		
		return categoryList;
	}
	
	public List<Category> getCategories() {
		return categories;
	}
	

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	
	public BigDecimal getPrice() {
		return price;
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
