package com.example.demo.entity;

import java.beans.Transient;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="order_items")
public class OrderItems {

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id private Long id;
	
	private String name;
	private String description;
	private BigDecimal price;
	private int quantity;
	private BigDecimal subTotal;
	
	@Type(type = "numeric_boolean")
	private boolean promoCodeUsed = false;

	@ManyToOne
	@JoinColumn(name = "order_id", nullable = false)
	@JsonIgnore
	private Orders order;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public BigDecimal getSubTotal() {
		return getPrice().multiply(new BigDecimal(getQuantity()));
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public boolean isPromoCodeUsed() {
		return promoCodeUsed;
	}

	public void setPromoCodeUsed(boolean promoCodeUsed) {
		this.promoCodeUsed = promoCodeUsed;
	}
	
	public Orders getOrder() {
		return order;
	}

	public void setOrder(Orders order) {
		this.order = order;
	}
}
