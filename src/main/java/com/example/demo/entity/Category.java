package com.example.demo.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.example.demo.dto.ItemDTO;

@Entity
public class Category {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id 
	private Long id;
	
	@NotBlank
	@Size(min=3, max=50)
	private String name;
	
	@NotBlank
	@Size(min=10, max=150)
	private String description;
	
	@ManyToMany(mappedBy = "categories")
	private List<Item> items;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<Item> getItems() {
		return items;
	}
	
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
	public List<ItemDTO> getItemDetails() {
		
		List<ItemDTO> itemList = new ArrayList<>();
		
		for(Item item : items) {
			ItemDTO newItem = new ItemDTO();
			
			newItem.setId(item.getId());
			newItem.setName(item.getName());
			newItem.setDescription(item.getDescription());
			newItem.setPrice(item.getPrice());
			newItem.setCategoryIds(item.getCategoryIds());
			
			itemList.add(newItem);
		}
		
		return itemList;
	}
}
