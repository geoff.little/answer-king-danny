package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PromoCode {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	private Long id;

	private String code;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPromoCode() {
		return code;
	}

	public void setPromoCode(String promoCode) {
		this.code = promoCode;
	}
}
