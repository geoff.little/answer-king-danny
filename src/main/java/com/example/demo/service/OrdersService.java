package com.example.demo.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.dto.ItemPopularityDTO;
import com.example.demo.dto.OrdersDTO;
import com.example.demo.entity.Item;
import com.example.demo.entity.OrderItems;
import com.example.demo.entity.Orders;
import com.example.demo.entity.PromoCode;
import com.example.demo.exceptions.OrderException;
import com.example.demo.exceptions.PromoCodeException;
import com.example.demo.exceptions.DoesNotExistException;
import com.example.demo.repository.ItemRepository;
import com.example.demo.repository.OrderItemsRepository;
import com.example.demo.repository.OrdersRepository;
import com.example.demo.repository.PromoCodeRepository;

@Service
public class OrdersService {

	@Autowired
	private OrdersRepository ordersRepository;

	@Autowired
	private OrderItemsRepository orderItemsRepository;

	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private PromoCodeRepository promoCodeRepository;

	@Autowired
	private ModelMapper modelMapper;

	public Collection getAll() {

		List<OrdersDTO> ordersList = new ArrayList<>();

		ordersRepository.findAll(Sort.by(Sort.Direction.DESC, "orderDateTime")).stream().forEach(order -> {

			OrdersDTO ordersDto = modelMapper.map(order, OrdersDTO.class);

			ordersList.add(ordersDto);
		});

		return ordersList;
	}

	public OrdersDTO getOrderById(Long id) throws DoesNotExistException {

		try {
			Orders currentOrder = ordersRepository.findById(id).get();

			OrdersDTO ordersDto = modelMapper.map(currentOrder, OrdersDTO.class);

			return ordersDto;
		} catch (NoSuchElementException e) {
			throw new DoesNotExistException("Given 'id' does not exist!");
		}
	}
	
	public Collection getOrdersBetweenDates(LocalDate startDate, LocalDate endDate) {
		
		List<OrdersDTO> ordersList = new ArrayList<>();
		
		LocalDateTime newStartDate = startDate.atTime(00,00);
		LocalDateTime newEndDate = endDate.atTime(23,59);
		
		ordersRepository.findByOrderStatusNotAndOrderDateTimeBetween("OPEN", newStartDate, newEndDate).stream().forEach(order -> {
			
			OrdersDTO ordersDto = modelMapper.map(order, OrdersDTO.class);

			ordersList.add(ordersDto);
			
		});
		
		return ordersList;
	}

	public OrdersDTO createOrder() {
		
		Orders ordersEntity = new Orders();
		LocalDateTime currentDateTime = LocalDateTime.now();

		ordersEntity.setOrderDateTime(currentDateTime);
		ordersEntity.setOrderStatus("OPEN");
		
		ordersEntity = ordersRepository.save(ordersEntity);
		
		OrdersDTO newOrder = modelMapper.map(ordersEntity, OrdersDTO.class);

		return newOrder;
	}

	@Transactional
	public OrdersDTO addItem(Long itemId, Long orderId, int quantity) throws OrderException, DoesNotExistException{

		Optional<Orders> order = ordersRepository.findById(orderId);
		if (!order.isPresent()) throw new DoesNotExistException("Order ID doesn't exist!");
			
		Orders o = order.get();
			
		if("CANCELLED".equals(o.getOrderStatus())) throw new OrderException("Cannot add item(s) to cancelled order!");

		Optional<Item> item = itemRepository.findById(itemId);
		if (!item.isPresent()) throw new DoesNotExistException("Item ID doesn't exist!"); 

		OrderItems orderItem = modelMapper.map(item.get(), OrderItems.class);
		
		OrderItems existingOrderItem = orderItemsRepository.findByOrderIdAndName(orderId, orderItem.getName());

		if (quantity == 0) {
			removeItem(existingOrderItem.getId());
			ordersRepository.save(o);
		}

		if (existingOrderItem != null) {
			existingOrderItem.setQuantity(quantity);
			orderItem = orderItemsRepository.save(existingOrderItem);
		}
		
		else {
			orderItem.setId(null); //Reset OrderItem ID.
			orderItem.setQuantity(quantity);
			orderItem.setSubTotal(orderItem.getSubTotal());
			orderItem.setOrder(o);

			orderItemsRepository.save(orderItem);
			checkPromoCodeUsed(orderId, orderItem.getId());

			o = ordersRepository.findById(orderId).get();
		}
		
		OrdersDTO ordersDto = modelMapper.map(o, OrdersDTO.class);
		
		return ordersDto;

	}

	@Transactional
	public void removeItem(Long orderItemId) throws OrderException{
		
		String currentOrderItemStatus = orderItemsRepository.findById(orderItemId).get().getOrder().getOrderStatus();
		Long orderId = orderItemsRepository.findById(orderItemId).get().getOrder().getId();
		
		if("CANCELLED".equals(currentOrderItemStatus)) throw new OrderException("Cannot remove items from cancelled order!");
		
		orderItemsRepository.deleteById(orderItemId);
	}
	
	@Transactional
	public void cancelOrder(Long orderId) throws DoesNotExistException {
		
		Optional<Orders> currentOrder = ordersRepository.findById(orderId);	

		if(!currentOrder.isPresent()) throw new DoesNotExistException("Order ID does not exist!");
		
		currentOrder.get().setOrderStatus("CANCELLED");
		
		ordersRepository.save(currentOrder.get());
	}
	
	@Transactional
	public OrdersDTO payOrder(Long orderId, BigDecimal paymentAmount) throws OrderException{
		
		Optional<Orders> paidOrder = ordersRepository.findById(orderId);
		BigDecimal orderPrice = paidOrder.get().getTotalPrice();
		
		if("COMPLETED".equals(paidOrder.get().getOrderStatus())) throw new OrderException("Cannot pay for a Completed order!");
		if("CANCELLED".equals(paidOrder.get().getOrderStatus())) throw new OrderException("Cannot pay for a Cancelled order!");
		
		paidOrder.get().setAmountPaid(paymentAmount);
		paidOrder.get().setOrderStatus("COMPLETED");
		ordersRepository.save(paidOrder.get());
		
		OrdersDTO paidOrderDto = modelMapper.map(paidOrder.get(), OrdersDTO.class);
		
		paidOrderDto.setTotalChange(paidOrder.get().getChange(orderPrice, paymentAmount));
		
		return paidOrderDto;
	}
	
	public Collection<ItemPopularityDTO>  getPopularity() {
		
		return orderItemsRepository.findPopularItems();
	}

	public void takeDiscount(Long orderId, String promoCode) throws PromoCodeException{
		
		Optional<PromoCode> currentCode = promoCodeRepository.findByCode(promoCode);
		if(!currentCode.isPresent()) throw new PromoCodeException("Promotional code invalid!");
		
		Orders currentOrder = ordersRepository.findById(orderId).get();
		if(currentOrder.isPromoCodeUsed() == true) throw new PromoCodeException("Discount already applied!");
		
		orderItemsRepository.findByOrderId(orderId).stream().forEach(item -> {
			
			BigDecimal currentPrice = item.getPrice();
			BigDecimal discount = new BigDecimal("2");
			BigDecimal newPrice = currentPrice.divide(discount);
			
			item.setPrice(newPrice);
			item.setPromoCodeUsed(true);
			
			orderItemsRepository.save(item);
		});		
		
		currentOrder.setPromoCodeUsed(true);
		
		ordersRepository.save(currentOrder);
	}
	
	public void checkPromoCodeUsed(Long orderId, Long orderItemId) {
		
		Orders currentOrder = ordersRepository.findById(orderId).get();
		
		if(currentOrder.isPromoCodeUsed() == true) {
			recalculateTotal(orderItemId);
		}
	}
	
	public void recalculateTotal(Long orderItemId) {
		
		if(orderItemsRepository.findById(orderItemId).get().isPromoCodeUsed() == false) {
			
			OrderItems item = orderItemsRepository.findById(orderItemId).get();
			
			BigDecimal currentPrice = item.getPrice();
			BigDecimal discount = new BigDecimal("2");
			BigDecimal newPrice = currentPrice.divide(discount);
			
			item.setPrice(newPrice);
			item.setPromoCodeUsed(true);
			
			orderItemsRepository.save(item);
		}
	}
}
