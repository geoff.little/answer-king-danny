package com.example.demo.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.dto.CategoryDTO;
import com.example.demo.dto.CreateCategoryDTO;
import com.example.demo.dto.UpdateCategoryDTO;
import com.example.demo.entity.Category;
import com.example.demo.exceptions.DoesNotExistException;
import com.example.demo.exceptions.ExistingNameException;
import com.example.demo.repository.CategoryRepository;

@Service
public class CategoryService {
	
	@Autowired 
	private CategoryRepository categoryRepository;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public Collection getAll() {
		
		List<CategoryDTO> categoryList = new ArrayList<>();
		
		categoryRepository.findAll(Sort.by(Sort.Direction.ASC, "name")).stream().forEach(category -> {
			CategoryDTO categoryDto = modelMapper.map(category, CategoryDTO.class);
			
			categoryDto.setItemDetails(category.getItemDetails());
			
			categoryList.add(categoryDto);
		});
		
		
		return categoryList;
	}
	
	public CategoryDTO getCategoryById(Long id) throws DoesNotExistException{
		
		try {
			Category currentCategory = categoryRepository.findById(id).get();
			
			CategoryDTO categoryDto = new CategoryDTO();
			BeanUtils.copyProperties(currentCategory, categoryDto);
			categoryDto.setItemDetails(currentCategory.getItemDetails());
			
			return categoryDto;
		}
		catch(NoSuchElementException e) {
			throw new DoesNotExistException("Given 'id' does not exist!");
		}
		
	}
	
	public List<Category> getByIds(List<Long> id) {
		
		return categoryRepository.findAllById(id);
	}
	
	public CategoryDTO createCategory(CreateCategoryDTO category) throws ExistingNameException {
		
		if(categoryRepository.countByName(category.getName()) > 0) {
			throw new ExistingNameException("Category name already exists!");
		}
		
		Category categoryEntity = new Category();
		BeanUtils.copyProperties(category, categoryEntity);
		
		categoryEntity = categoryRepository.save(categoryEntity);
		
		CategoryDTO categoryDto = new CategoryDTO();
		BeanUtils.copyProperties(categoryEntity, categoryDto);
		
		return categoryDto;
	}
	
	public UpdateCategoryDTO updateCategory(Long id, UpdateCategoryDTO updateCategory) throws DoesNotExistException {
		try {
			
			Category existingCategory = categoryRepository.findById(id).get();
			
			existingCategory.setName(updateCategory.getName());
			existingCategory.setDescription(updateCategory.getDescription());
			
			Category updatedCategory = categoryRepository.save(existingCategory);
			UpdateCategoryDTO newUpdatedCategory = new UpdateCategoryDTO();
			
			BeanUtils.copyProperties(updatedCategory, newUpdatedCategory);
			
			return newUpdatedCategory;
			
		}
		catch(NoSuchElementException e) {
			throw new DoesNotExistException("Given 'id' does not exist!");
		}
	}
	
	public void deleteCategory(Long id) throws DoesNotExistException{
		try {
			categoryRepository.deleteById(id);
		}
		catch(NoSuchElementException e) {
			throw new DoesNotExistException("Given 'id' does not exist!");
		}
	}
}

