package com.example.demo.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.dto.CreateItemDTO;
import com.example.demo.dto.ItemDTO;
import com.example.demo.dto.UpdateItemDTO;
import com.example.demo.entity.Category;
import com.example.demo.entity.Item;
import com.example.demo.entity.OrderItems;
import com.example.demo.entity.Orders;
import com.example.demo.exceptions.DoesNotExistException;
import com.example.demo.exceptions.ExistingNameException;
import com.example.demo.repository.ItemRepository;

@Service
public class ItemService {
	
	@Autowired 
	private ItemRepository itemRepository;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public Collection getAll() {
		
		List<ItemDTO> itemList = new ArrayList<>();
		
		itemRepository.findAll(Sort.by(Sort.Direction.ASC, "name")).stream().forEach(item -> {			
			ItemDTO itemDto = new ItemDTO();
			
			BeanUtils.copyProperties(item, itemDto);
			itemDto.setCategoryIds(item.getCategoryIds());
			
			itemList.add(itemDto);
		});
		
		return itemList;
	}
	
	public ItemDTO getItemById(Long id) throws DoesNotExistException {
		
		try {
			Item currentItem = itemRepository.findById(id).get();
			
			ItemDTO itemDto = new ItemDTO();
			BeanUtils.copyProperties(currentItem, itemDto);
			itemDto.setCategoryIds(currentItem.getCategoryIds());
			
			return itemDto;
		}
		catch(NoSuchElementException e){
			throw new DoesNotExistException("Given 'id' does not exist!");
		}
		
	}
	
	
	public ItemDTO createItem(CreateItemDTO item) throws ExistingNameException{
		
		if(itemRepository.countByName(item.getName()) > 0) {
			throw new ExistingNameException("Item name already exists!");
		}
		
		List<Category> categories = categoryService.getByIds(item.getCategoryId());
		
		Item itemEntity = new Item();
		BeanUtils.copyProperties(item, itemEntity);
		
		itemEntity.setCategories(categories);
		
		itemEntity = itemRepository.save(itemEntity);
		
		ItemDTO itemDto = new ItemDTO();
		BeanUtils.copyProperties(itemEntity, itemDto);
		
		return itemDto;
	}
	
	public ItemDTO updateItem(Long id, UpdateItemDTO updateItem) throws DoesNotExistException {
		try {
			Item existingItem = itemRepository.findById(id).get();
			List<Category> categories = categoryService.getByIds(updateItem.getCategoryId());
			
			existingItem.setName(updateItem.getName());
			existingItem.setDescription(updateItem.getDescription());
			existingItem.setPrice(updateItem.getPrice());
			existingItem.setCategories(categories);
			
			Item updatedItem = itemRepository.save(existingItem);
			ItemDTO newUpdatedItem = new ItemDTO();
			
			BeanUtils.copyProperties(updatedItem, newUpdatedItem);
			
			newUpdatedItem.setCategoryIds(updatedItem.getCategoryIds());
			
			return newUpdatedItem;
		}
		catch(NoSuchElementException e) {
			throw new DoesNotExistException("Given 'id' does not exist!");
		}
	}
	
	public void deleteItem(Long id) throws DoesNotExistException{
		try {
			itemRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new DoesNotExistException("Given 'id' does not exist!");
		}
	}
}
