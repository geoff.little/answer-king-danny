package com.example.demo.repository;

import java.time.LocalDateTime;
import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Orders;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Long> {
	
	public Collection<Orders> findByOrderStatusNotAndOrderDateTimeBetween(String status, LocalDateTime startDate, LocalDateTime endDate);
}
