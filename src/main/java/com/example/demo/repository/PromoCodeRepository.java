package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.PromoCode;


@Repository
public interface PromoCodeRepository extends JpaRepository<PromoCode, Long> {
	
	public Optional<PromoCode> findByCode(String code);

}
