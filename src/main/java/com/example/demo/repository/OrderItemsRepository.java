package com.example.demo.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.ItemPopularityDTO;
import com.example.demo.entity.OrderItems;

@Repository
public interface OrderItemsRepository extends JpaRepository<OrderItems, Long>{
	
	public OrderItems findByOrderIdAndName(Long orderId, String name);
	
	public Collection<OrderItems> findByOrderId(Long orderId);
	
	@Query(value="SELECT new com.example.demo.dto.ItemPopularityDTO(name, description, price, SUM(quantity))" + 
			"FROM OrderItems " + 
			"GROUP BY name " + 
			"ORDER BY SUM(quantity) desc")
	public Collection<ItemPopularityDTO> findPopularItems();
}
