package com.example.demo.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CategoryDTO;
import com.example.demo.dto.CreateItemDTO;
import com.example.demo.dto.ItemDTO;
import com.example.demo.dto.UpdateItemDTO;
import com.example.demo.entity.Item;
import com.example.demo.exceptions.DoesNotExistException;
import com.example.demo.exceptions.ExistingNameException;
import com.example.demo.service.ItemService;

@RestController
@RequestMapping("/item")
public class ItemController {

	@Autowired
	private ItemService itemService;
	
	@PostMapping
	public @ResponseBody ItemDTO createItem(@Valid @RequestBody CreateItemDTO item) throws ExistingNameException{
		return itemService.createItem(item);
	}
	
	@GetMapping
	public @ResponseBody Collection<ItemDTO> getAllItems(){
		return itemService.getAll();
	}
	
	@GetMapping("/{id}")
	public @ResponseBody ItemDTO getItem(@PathVariable Long id) throws DoesNotExistException {
		return itemService.getItemById(id);
	}
	
	@DeleteMapping("/{id}")
	public void deleteItem(@PathVariable Long id) throws DoesNotExistException {
		itemService.deleteItem(id);
	}
	
	@PutMapping("/{id}")
	public ItemDTO updateItem(@PathVariable Long id, @RequestBody UpdateItemDTO itemDto) throws DoesNotExistException, ExistingNameException{
		return itemService.updateItem(id, itemDto);
	}
}
