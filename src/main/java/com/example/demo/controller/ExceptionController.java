package com.example.demo.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.demo.exceptions.DoesNotExistException;
import com.example.demo.exceptions.ExistingNameException;
import com.example.demo.exceptions.OrderException;
import com.example.demo.exceptions.PromoCodeException;
import com.example.demo.util.FieldErrorMessage;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ConstraintViolationException.class)
	public @ResponseBody ResponseEntity<Object> handleMethodArgumentNotValid(ConstraintViolationException e){
		
		List<FieldErrorMessage> fieldErrorMessages = e.getConstraintViolations().stream().map(cv -> new FieldErrorMessage(cv.getPropertyPath().toString(), cv.getMessage())).collect(Collectors.toList());
	
		return new ResponseEntity<Object>(fieldErrorMessages, HttpStatus.BAD_REQUEST);
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(TransactionSystemException.class)
	public @ResponseBody ResponseEntity<Object> handleTransactionSystemException(TransactionSystemException e){

		return handleMethodArgumentNotValid((ConstraintViolationException)getConstraintViolationException(e));
	}
	
	private Throwable getConstraintViolationException(Throwable parent) {
		Throwable child  = parent.getCause();
		if(child instanceof ConstraintViolationException) {
			return child;
		} else {
			return getConstraintViolationException(child);
		}
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ExistingNameException.class)
	public @ResponseBody FieldErrorMessage handleExistingNameException(ExistingNameException e){
	
		return new FieldErrorMessage("name" , e.getMessage());
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(DoesNotExistException.class)
	public @ResponseBody FieldErrorMessage handleDoesNotExistException(DoesNotExistException e){
	
		return new FieldErrorMessage("id" , e.getMessage());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(OrderException.class)
	public @ResponseBody FieldErrorMessage handleDoesNotExistException(OrderException e){
	
		return new FieldErrorMessage("order_status" , e.getMessage());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(PromoCodeException.class)
	public @ResponseBody FieldErrorMessage handlePromoCodeException(PromoCodeException e){
	
		return new FieldErrorMessage("promocode" , e.getMessage());
	}
}
