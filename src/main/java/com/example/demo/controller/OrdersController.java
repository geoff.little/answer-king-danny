package com.example.demo.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ItemPopularityDTO;
import com.example.demo.dto.OrdersDTO;
import com.example.demo.exceptions.DoesNotExistException;
import com.example.demo.exceptions.OrderException;
import com.example.demo.exceptions.PromoCodeException;
import com.example.demo.service.OrdersService;

@RestController
@RequestMapping("/Order")
public class OrdersController {
	
	@Autowired
	private OrdersService ordersService;

	@GetMapping
	public @ResponseBody Collection<OrdersDTO> getAllOrders() {
		return ordersService.getAll();
	}
	
	@GetMapping("/{id}")
	public @ResponseBody OrdersDTO getOrder(@PathVariable Long id) throws DoesNotExistException {
		return ordersService.getOrderById(id);
	}
	
	@GetMapping("/ItemPopularity")
	public @ResponseBody Collection<ItemPopularityDTO> getPopularity() {
		return ordersService.getPopularity();
	}
	
	@GetMapping("/BetweenDates")
	public @ResponseBody Collection<OrdersDTO> getOrdersBetweenDates(@RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate startDate, 
																	 @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate endDate) {
		return ordersService.getOrdersBetweenDates(startDate, endDate);
	}
	
	@PostMapping("/CreateOrder")
	public @ResponseBody OrdersDTO createOrder() {
		return ordersService.createOrder();
	}
	
	@PostMapping("/CancelOrder")
	public void cancelOrder(Long orderId) throws DoesNotExistException {
		ordersService.cancelOrder(orderId);
	}
	
	@PostMapping("/PayOrder/{orderId}")
	public @ResponseBody OrdersDTO payOrder(@PathVariable Long orderId, 
											@Valid @NumberFormat(pattern = "#0,00") @RequestParam BigDecimal paymentAmount) throws OrderException{
		return ordersService.payOrder(orderId, paymentAmount);
	}
	
	@PostMapping("/PayOrder/{orderId}/Discount")
	public void takeDiscount(@RequestParam Long orderId, @RequestParam String promoCode) throws PromoCodeException{
		ordersService.takeDiscount(orderId, promoCode);
	}
	
	@PostMapping("/AddItem/{orderId}")
	public @ResponseBody OrdersDTO addItem(@PathVariable Long orderId,  @RequestParam Long itemId, @RequestParam int quantity) throws OrderException, DoesNotExistException {
		
		return ordersService.addItem(itemId, orderId, quantity);
	}
	
	@DeleteMapping("/RemoveItem/{orderItemId}")
	public void removeItem(@PathVariable Long orderItemId) throws OrderException{
		
		ordersService.removeItem(orderItemId);
	}
}
