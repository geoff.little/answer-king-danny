package com.example.demo.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CategoryDTO;
import com.example.demo.dto.CreateCategoryDTO;
import com.example.demo.dto.UpdateCategoryDTO;
import com.example.demo.exceptions.DoesNotExistException;
import com.example.demo.exceptions.ExistingNameException;
import com.example.demo.service.CategoryService;

@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@PostMapping
	public @ResponseBody CategoryDTO createCategory(@Valid @RequestBody CreateCategoryDTO category) throws ExistingNameException{
		return categoryService.createCategory(category);
	}
	
	@GetMapping
	public @ResponseBody Collection<CategoryDTO> getAllCategories(){
		return categoryService.getAll();
	}
	
	@GetMapping("/{id}")
	public @ResponseBody CategoryDTO getCategory(@PathVariable Long id) throws DoesNotExistException {
		return categoryService.getCategoryById(id);
	}
	
	@DeleteMapping("/{id}")
	public void deleteCategory(@PathVariable Long id) throws DoesNotExistException {
		categoryService.deleteCategory(id);
	}
	
	@PutMapping("/{id}")
	public UpdateCategoryDTO updateCategory(@PathVariable Long id, @RequestBody UpdateCategoryDTO categoryDto) throws DoesNotExistException, ExistingNameException {
		return categoryService.updateCategory(id, categoryDto);
	}
}
