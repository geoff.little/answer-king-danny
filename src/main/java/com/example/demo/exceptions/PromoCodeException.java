package com.example.demo.exceptions;

public class PromoCodeException extends Exception {

	public PromoCodeException(String err) {
		super(err);
	}

}
