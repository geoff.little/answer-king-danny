package com.example.demo.exceptions;

public class DoesNotExistException extends Exception {

	public DoesNotExistException(String err) {
		super(err);
	}

}
