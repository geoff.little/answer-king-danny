package com.example.demo.exceptions;

public class ExistingNameException extends Exception {

	public ExistingNameException(String err) {
		super(err);
	}

}
