package com.example.demo.dto;

import java.math.BigDecimal;

public class ItemPopularityDTO {

	private String name;
	private String description;
	private BigDecimal price;
	private Long timesOrdered;
	
	public ItemPopularityDTO(String name, String description, BigDecimal price, Long timesOrdered) {
		super();
		this.name = name;
		this.description = description;
		this.price = price;
		this.timesOrdered = timesOrdered;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public BigDecimal getPrice() {
		return price;
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public Long getTimesOrdered() {
		return timesOrdered;
	}

	public void setTimesOrdered(Long timesOrdered) {
		this.timesOrdered = timesOrdered;
	}
}
