package com.example.demo.dto;

import java.util.List;

public class CategoryDTO {
	
	private Long id;
	private String name;
	private String description;
	private List<ItemDTO> itemDetails;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<ItemDTO>  getItemDetails() {
		return itemDetails;
	}

	public void setItemDetails(List<ItemDTO>  items) {
		this.itemDetails = items;
	}
}
