package com.example.demo.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class OrdersDTO {
	
	private Long id;
	private LocalDateTime orderDateTime;
	private String orderStatus;
	private BigDecimal totalPrice;
	
	@JsonInclude(Include.NON_NULL)
	private BigDecimal amountPaid;
	
	@JsonInclude(Include.NON_NULL)
	private BigDecimal totalChange;
	
	private List<OrderItemsDTO> orderItemsDetails;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public LocalDateTime getOrderDateTime() {
		return orderDateTime;
	}
	
	public void setOrderDateTime(LocalDateTime orderDateTime) {
		this.orderDateTime = orderDateTime;
	}
	
	public String getOrderStatus() {
		return orderStatus;
	}
	
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	public BigDecimal getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	public BigDecimal getTotalChange() {
		return totalChange;
	}

	public void setTotalChange(BigDecimal totalChange) {
		this.totalChange = totalChange;
	}

	public List<OrderItemsDTO> getOrderItemsDetails() {
		return orderItemsDetails;
	}
	
	public void setOrderDetails(List<OrderItemsDTO> orderItemsDetails) {
		this.orderItemsDetails = orderItemsDetails;
	}
}
