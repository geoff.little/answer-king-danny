package com.example.demo.dto;

import java.math.BigDecimal;
import java.util.List;

public class UpdateItemDTO {
	
	private String name;
	private String description;
	private List<Long> categoryIds;
	private BigDecimal price;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<Long> getCategoryId() {
		return categoryIds;
	}

	public void setCategoryId(List<Long> categoryId) {
		this.categoryIds = categoryId;
	}
	
	public BigDecimal getPrice() {
		return price;
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
